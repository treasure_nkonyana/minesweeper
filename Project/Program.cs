﻿using Minesweeper.Library.Helper;
using Minesweeper.Library.MinesweeprLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    class Program
    {
        static void Main(string[] args)
        {
            if (File.Exists(Global.FILE_PATH))
            {
                Field field = new Field();
                Console.Write(field.EvaluateByCalculateGridField());
            }
            else
            {
                Console.WriteLine("Minesweeper.txt file does not exist in my documens");
            }
        }
    }
}
