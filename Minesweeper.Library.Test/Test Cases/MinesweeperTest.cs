﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Minesweeper.Library.MinesweeprLogic;

namespace Minesweeper.Library.Test.Test_Cases
{
    public class MinesweeperTest
    {
        private Field field;
        [Fact]
        public void ReadRowsAndColumnstFromInputTextFile_CanReadRowsAndColumnsFromTextFile()
        {
            //Arrange
            field = new Field();
            int expectRows = 3;
            int expectedCols = 5;
            //Assert
            Assert.Equal(expectRows, field.rows);
            Assert.Equal(expectedCols, field.cols);
        }
        [Fact]
        public void SaveTextFileContentToCharArray_CanReadContentToCharArray()
        {
            //Arrange
            field = new Field();
            field.ReadContentFromInputTextFile();
            //Act
            field.SaveTextFileContentToCharArray();
            //Assert
        }

        [Fact]
        public void GetContentFromTextFile_CanGetContentFromTheMinesweeperTextFile()
        {
            //Arrange
            field = new Field();
            string expected = "**...\n.....\n.*...";
            //Act
            string actual = field.GetContentFromTextFile();
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EvaluateByCalculateGridField()
        {
            //Arrange
            field = new Field();
            string expected = "**100\n33200\n1*100";
            //Act
            string actual = field.EvaluateByCalculateGridField();
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
