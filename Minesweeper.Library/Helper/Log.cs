﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minesweeper.Library.Helper
{
    public class Log
    {
        private static bool isDebugOn = false;
        public static void Debug(string tag, string message)
        {
            if(isDebugOn)
            {
                Console.WriteLine(tag + " | " + message);
            }
        }
        public static void Error(string tag, string message)
        {
            if (isDebugOn)
            {
                Console.Error.WriteLine(tag + " | " + message);
            }
        }
    }
}
