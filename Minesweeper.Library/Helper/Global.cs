﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minesweeper.Library.Helper
{
    public class Global
    {
        private static string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        public static readonly string FILE_PATH = System.IO.Path.Combine(folderPath, "Minesweeper.txt");
        public const char MINE = '*';
    }
}
