﻿using Minesweeper.Library.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minesweeper.Library.MinesweeprLogic
{
    public class Field
    {
        private string TAG = typeof(File).Name;
        public int rows { private set; get; }
        public int cols { private set; get; }
        private string[] line;
        private string response = string.Empty;
        private char[,] inputFieldArray;
        private int[,] evaluatedFieldArray;
        public Field()
        {
            ReadContentFromInputTextFile();
            inputFieldArray = new char[rows, cols];
            evaluatedFieldArray = new int[rows,cols];
            SaveTextFileContentToCharArray();
        }
        public void ReadContentFromInputTextFile()
        {
            try
            {
                Log.Debug(TAG, "START | ReadContentFromInputTextFile");
                if (File.Exists(Global.FILE_PATH))
                {
                    using (var streamReader = new StreamReader(Global.FILE_PATH))
                    {
                        line = streamReader.ReadLine().Split();
                        rows = Convert.ToInt32(line.ElementAt(0));
                        cols = Convert.ToInt32(line.ElementAt(1));
                        response = streamReader.ReadToEnd();
                    }
                }
                Log.Debug(TAG, "END | ReadContentFromInputTextFile");
            }
            catch(Exception ex)
            {
                Log.Error(TAG, "ERR | ReadContentFromInputTextFile " + ex.Message);
            }
        }

        public void SaveTextFileContentToCharArray()
        {
            try
            {
                Log.Debug(TAG, " START | SaveTextFileContentToCharArray");
                using (var streamR = new StreamReader(Global.FILE_PATH))
                {
                    streamR.ReadLine();
                    for (int row = 0; row < rows; row++)
                    {
                        string test = streamR.ReadLine();
                        for (int col = 0; col < cols; col++)
                        {
                            inputFieldArray[row, col] = test[col];
                        }
                    }
                }
                Log.Debug(TAG, " END | SaveTextFileContentToCharArray");
            }
            catch(Exception ex)
            {
                Log.Debug(TAG, " START | SaveTextFileContentToCharArray " + ex.Message);
            }
        }

        public string GetContentFromTextFile()
        {
            string output = string.Empty;
            try
            {
                Log.Debug(TAG, " START | SaveTextFileContentToCharArray");
                for (int row = 0; row < rows; row++)
                {
                    for (int col = 0; col < cols; col++)
                    {
                        output +=inputFieldArray[row, col];
                    }
                    output += "\n";
                }
                Log.Debug(TAG, " END | SaveTextFileContentToCharArray");
            }
            catch (Exception ex)
            {
                Log.Debug(TAG, " START | SaveTextFileContentToCharArray " + ex.Message);
            }
            return output.Trim();
        }

        public string EvaluateByCalculateGridField()
        {
            string response = string.Empty;
            try
            {
                Log.Debug(TAG, " START | EvaluateByCalculateGridField");
                for (int row = 0; row < rows; row++)
                {
                    for (int col = 0; col < cols; col++)
                    {
                        if (inputFieldArray[row, col] == Global.MINE)
                        {
                            UpperBoundMine(row, col);
                            MiddleMine(row, col);
                            BottomBoundMine(row, col);
                        }
                    }
                }
                response = GetNumberOfMineResponse(response);
                Log.Debug(TAG, " END | EvaluateByCalculateGridField");
            }
            catch (Exception ex)
            {
                Log.Debug(TAG, " ERR | EvaluateByCalculateGridField " + ex.Message);
            }
            return response.Trim();
        }

        private string GetNumberOfMineResponse(string response)
        {
            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < cols; col++)
                {
                    if (inputFieldArray[row, col] == Global.MINE)
                    {
                        response += Global.MINE;
                    }
                    else
                    {
                        response += evaluatedFieldArray[row, col];
                    }
                }
                response += "\n";
            }

            return response;
        }

        private void BottomBoundMine(int row, int col)
        {
            try
            {
                Log.Debug(TAG, "START | BottomBoundMine");
                if (row + 1 < rows && col > 0)
                    evaluatedFieldArray[row + 1, col - 1]++;
                if (row + 1 < rows && col + 1 < cols)
                    evaluatedFieldArray[row + 1, col + 1]++;
                if (row + 1 < rows)
                    evaluatedFieldArray[row + 1, col]++;
                Log.Debug(TAG, "END | BottomBoundMine");
            }
            catch(Exception ex)
            {
                Log.Debug(TAG, "ERR | BottomBoundMine " + ex.Message);
            }
        }

        private void MiddleMine(int row, int col)
        {
            try
            {
                Log.Debug(TAG, "START | MiddleMine");
                if (col > 0)
                    evaluatedFieldArray[row, col - 1]++;
                if (col + 1 < cols)
                    evaluatedFieldArray[row, col + 1]++;
                Log.Debug(TAG, "END | MiddleMine");
            }
            catch(Exception ex)
            {
                Log.Error(TAG, "ERR | MiddleMine " + ex.Message);
            }
        }

        private void UpperBoundMine(int row, int col)
        {
            try
            {
                Log.Debug(TAG, "SART | UpperBoundMine");
                if (row > 0 && col > 0)
                    evaluatedFieldArray[row - 1, col - 1]++;
                if (row > 0)
                    evaluatedFieldArray[row - 1, col]++;
                if (row > 0 && col + 1 < cols)
                    evaluatedFieldArray[row - 1, col + 1]++;
                Log.Debug(TAG, "SART | UpperBoundMine");
            }
            catch(Exception ex)
            {
                Log.Error(TAG, "ERR | UpperBoundMine " + ex.Message);
            }
        }
    }
}
